#!/bin/bash
#
# Purpose: setup the 'bitbake' docker to use ./{build|sstate|image}
#
# Notes:
#   docker --e environment pairs (can have multiple)
#           -p ports (host:port_h:port_c|port_h:port_c|port_c)
#           -v non-tracked (for changes) folders, possibly from the host.
#               -v [host_path:]c_path[:(ro|rw)]
#               -volumes-from CONTAINER
#

### setup things so we know where we are ###
HERE=`pwd`
HERE=${HERE%/bin}
WHO=`whoami`
if [[ -n "$SUDO_USER" ]]; then
    # we're in sudo. Find the caller's $HOME
    export HOME=$(bash <<< "echo ~$SUDO_USER");
    WHO=$SUDO_USER
fi
GROUP=`ls -dg $HERE | cut -d ' ' -f 3`

# source the conf/local.conf if it exists.
if [ -f $HERE/conf/local.conf ]; then
    . $HERE/conf/local.conf
fi
BS_CONF=${BS_CONF:-$HERE/conf}

### use getopts for handling options ###
BS_ENV_SET=""
BB_CONTINUE=""
while getopts ":hixke:c:p:t:" opt; do
    case $opt in
        c)
            BS_CONFIG=${OPTARG#conf/}
            echo "Using config file: $BS_CONFIG" >&2
            source "${BS_CONF}/${BS_CONFIG}"
            rc=$?
            if [ ! $rc = 0 ]; then
                echo "Could not source '$OPTARG'!"
                exit 1;
            fi
            ;;
        p)
            echo "Setting BB_PACKAGE to: $OPTARG" >&2
            PKG_OVERRIDE="$OPTARG"
            ;;
        e)
            echo "Setting $OPTARG"
            BS_ENV_SET="${BS_ENV_SET} -e ${OPTARG}"
            ;;
        k)
            echo "Passing -k (--continue) to bitbake"
            BB_CONTINUE=" -k "
            ;;
        x)
            echo "Echoing run..." >&2;
            RUN_ECHO=1
            ;;
        i)
            echo "Running interactive shell" >&2
            RUN_INTERACTIVE=1
            ;;
        t)
            echo "Extending Tuple with: $OPTARG" >&2
            TUPLE_EXT="-$OPTARG"
            ;;
        h)
            echo "run-builder usage:" ;
            echo " -h : this help" ;
            echo " -x : echo the resulting behaivor, don't execute." ;
            echo " -c : configuration file to source for environment." ;
            echo " -i : run interactive shell for this configuration." ;
            echo " -p : package/command for bitbake. Overrides BB_PACKAGE." ;
            echo " -t : tuple extension, e.g. 'mine' results in 'x-x-x-mine'" ;
            echo " -e : set an environment variable in the container.  e.g. THIS=THAT" ;
            echo " -k : pass the -k (--continue) argument to bitbake" ;
            exit 0
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
        \?)
            echo "Unknown option -$OPTARG" >&2
            ;;
    esac
done

### Bitbake Configuration ###
BB_DISTRO=${BB_DISTRO:-distro}
BB_DISTRO_TYPE=${BB_DISTRO_TYPE:-release}
BB_MACHINE=${BB_MACHINE:-machine}
# setup TUPLE
BS_TUPLE="$BB_DISTRO-$BB_DISTRO_TYPE-$BB_MACHINE$TUPLE_EXT"
# Paths.
BS_BIN=${BS_BIN:-$HERE/bin}
BS_CONF=${BS_CONF:-$HERE/conf}
BS_BUILD=${BS_BUILD:-$HERE/build}
BS_DL=${BS_DL:-$HERE/dl}
BS_SSTATE=${BS_SSTATE:-$HERE/sstate}
BS_IMAGE=${BS_IMAGE:-$HERE/images}
BS_ETC=${BS_ETC:-$HERE/etc}

### Create dir, to be sure it exists. ###
if [ ! -d $BS_BUILD/$BS_TUPLE ]; then 
    echo "making $BS_BUILD/$BS_TUPLE"
    install -v -m 3774 -o $WHO -g $GROUP -d $BS_BUILD/$BS_TUPLE
fi
if [ ! -d $BS_SSTATE/$BS_TUPLE ]; then 
    echo "making $BS_SSTATE/$BS_TUPLE"
    install -v -m 3774 -o $WHO -g $GROUP -d $BS_SSTATE/$BS_TUPLE
fi
if [ ! -d $BS_IMAGE/$BS_TUPLE ]; then 
    echo "making $BS_IMAGE/$BS_TUPLE"
    install -v -m 3774 -o $WHO -g $GROUP -d $BS_IMAGE/$BS_TUPLE
fi

### setup RUN_* behavior ###
if [ "$RUN_ECHO" = "1" ]; then
    RUN_ECHO='echo'
fi
RUN_CMD="bakethat $PKG_OVERRIDE $BB_CONTINUE"
if [ "$RUN_INTERACTIVE" = "1" ]; then
    RUN_OPTS='-i -t'
    RUN_CMD='exec >/dev/tty 2>/dev/tty </dev/tty && /bin/bash'
fi

### setup container default -- changeable for testing/debug ###
CONTAINER=${CONTAINER:-bitbake}

### Container Execution ###
echo "-- Starting '$CONTAINER' container... --"
echo "-- $BS_TUPLE --"
TMPDIR=$(mktemp -d)
$RUN_ECHO docker run $RUN_OPTS \
    -v $HOME:$HOME \
    -v $BS_ETC:/mnt/bitbake/etc:ro \
    -v $BS_SSTATE/$BS_TUPLE:/mnt/bitbake/sstate \
    -v $BS_CONF:/mnt/bitbake/conf:ro \
    -v $BS_DL:/mnt/bitbake/dl \
    -v $BS_BIN:/mnt/bitbake/bin:ro \
    -v $BS_BUILD/$BS_TUPLE:/mnt/bitbake/build \
    -v $BS_IMAGE/$BS_TUPLE:/mnt/bitbake/image \
    -v $TMPDIR:/tmp \
    -e HOME=$HOME \
    -e SHELL=/bin/bash \
    -e PATH=/mnt/bitbake/bin:/usr/bin:/usr/sbin:/sbin:/bin \
    -e BS_CONFIG=$BS_CONFIG \
    -e BS_TUPLE=$BS_TUPLE \
    -e BB_THREADS=$BB_THREADS \
    $BS_ENV_SET \
    -w /mnt/bitbake \
    --rm \
    -u $(id -u $WHO):$(id -g $WHO) \
     $CONTAINER \
    /bin/bash -c "$RUN_CMD"
RC=$?
echo "-- Stopped '$CONTAINER' containter. --"
echo "-- $BS_TUPLE --"
exit $RC 
